#include "loader.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "cities.h"

#define LINE_BUF_SIZE 400
#define DB_PATH "db.csv"

city readLine(char* line) {
  city ret;
  
  unsigned int i, col;
  char c;
  char* str = NULL;
  char* substr;
  size_t length;
  
  length = strlen(line);
  col = 0;
  for (i = 0; i < length; i++) {
    if (str == NULL) {
      str = &line[i];
    }
    
    c = line[i];
    if (c == ',' || c == '\n') {
      line[i] = '\0';
      
      if (col <= 2) {
        substr = (char*)malloc(strlen(str)+1);
        strcpy(substr, str);
      }
      
      switch (col) {
        case 0: /* Country */
        ret.country = substr;
        break;
        
        case 1: /* City (id) */
        ret.id = substr;
        break;
        
        case 2: /* AccentCity (name) */
        ret.name = substr;
        break;
        
        case 5: /* Latitude */
        ret.latitude = atof(str);
        break;
        
        case 6: /* Longitude */
        ret.longitude = atof(str);
        break;
      }
      
      str = NULL;
      col++;
    }
  }
  
  return ret;
}

citiesDB citiesDB_load() {  
  citiesDB db;
  city* buf = malloc(4000000 * sizeof(city));
  int count = 0;
  FILE* dbFile;
  char lineBuf[LINE_BUF_SIZE];
  clock_t t;
  
  printf("------------------------------\n");
  printf("loading database..");

  t = clock();
  dbFile = fopen(DB_PATH, "r");
    
  /* on saute la premiere ligne du fichier */
  fgets(lineBuf, LINE_BUF_SIZE, dbFile);
  
  while(fgets(lineBuf, LINE_BUF_SIZE, dbFile) != NULL) {
    
    if (count % 500000 == 0) {
      printf(".");
      fflush(stdout);
    }
    
    buf[count] = readLine(lineBuf);
    count++;
  }
  
  fclose(dbFile);
  
  db = citiesDB_create();
  citiesDB_fill(db, count, buf);
  
  free(buf);
  
  t = clock() - t;
  printf("\nloading complete. (time: %lf sec)\n", (double)t / CLOCKS_PER_SEC);
  
  return db;
}
