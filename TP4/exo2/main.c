#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "city.h"
#include "cities.h"
#include "loader.h"
#include "time.h"

int main() {
  citiesDB db = citiesDB_load();
  city c;
  char buf[100];
  clock_t t;
  
  while (1) {
    scanf("%100s", buf);
    if (!strcmp(buf, "/q")) {
      printf("Quit.\n");
      exit(EXIT_SUCCESS);
    }
    else {
      t = clock();
      if (citiesDB_get(db, buf, &c)) {
        city_print(c);
        
        t = clock() - t;
        printf("request time: %ld ticks\n", t);
      }
      else {
        printf("id '%s' not found.\n", buf);
      }
    }
  }
  
  return EXIT_SUCCESS;
}