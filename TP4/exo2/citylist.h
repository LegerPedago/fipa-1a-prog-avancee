#ifndef CITYLIST_H
#define CITYLIST_H

#include "city.h"

/* Definition du type de liste chaine de villes. */
typedef struct citynode* citylist;

/* 	@ensures: Retourne une liste vide. */
citylist citylist_create();

/* 	@ensures: Retourne un entier non nul si la liste est vide, 0 sinon. */
int citylist_isEmpty(citylist l);

/* 	@requires: l bien formee, value.id non NULL.
	@assigns: l
	@ensures: Ajoute value a la tete de la liste et retourne la
		nouvelle liste. */
citylist citylist_add(citylist l, city value);

/*	@requires: l bien formee, id non NULL.
	@assigns: l
	@ensures: Retire la premiere occurence de ville correspondante
		a l'id passe en parametre. Retourne la nouvelle liste. */
citylist citylist_remove(citylist l, const char* id);

/*	@requires: l bien formee, id non NULL.
	@assigns: ret
	@ensures: Assigne la premiere occurence de la ville correspondante
		a l'id passe en parametre dans le contenu de ret si le pointeur
		n'est pas NULL.
		Retourne un entier non nul si l'occurence existe, 0 sinon. */
int citylist_get(citylist l, const char* id, city* ret);

#endif