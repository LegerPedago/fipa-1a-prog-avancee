#ifndef LOADER_H
#define LOADER_H

#include "cities.h"

/*	@ensures: Retourne une table de ville preremplie. */
citiesDB citiesDB_load();

#endif