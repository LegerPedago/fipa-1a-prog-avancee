#include <stdio.h>
#include <stdlib.h>

#include "citylist.h"

int main() {
  city c, test1, test2, test1bis;
  citylist l = citylist_create();
  
  c.id = "test1";
  c.name = "TEST1";
  c.country = "testland";
  c.longitude = 15.0;
  c.latitude = 10.0;
  test1 = city_clone(c);
  
  printf("adding:\n");
  city_print(c);
  l = citylist_add(l, c);
  printf("\nOK\n");
  printf("------------------------------------\n");


  c.id = "test2";  
  c.name = "TEST2";
  c.longitude = 5.0;
  test2 = city_clone(c);

  printf("adding:\n");
  city_print(c);  
  l = citylist_add(l, c);
  printf("\nOK\n");
  printf("------------------------------------\n");

  
  c.id = "test1";
  c.name = "TEST1BIS";
  c.latitude = 22.0;
  test1bis = city_clone(c);
  
  printf("adding:\n");
  city_print(c);
  l = citylist_add(l, c);
  printf("\nOK\n");
  printf("------------------------------------\n");


  printf("getting test3:\n");
  if (citylist_get(l, "test3", &c)) {
    printf("TEST FAILED: test3 should not be found.\n");
    return EXIT_FAILURE;
  }
  
  printf("\nOK\n");
  printf("------------------------------------\n");


  printf("getting test2:\n");
  if (!citylist_get(l, "test2", &c)) {
    printf("TEST FAILED: test2 not found.\n");
    return EXIT_FAILURE;
  }
  if (!city_equals(c, test2)) {
    printf("TEST FAILED: cities are not equals\n");
    city_print(c);
    printf("\tand\n");
    city_print(test2);
    return EXIT_FAILURE;
  }
  
  city_print(c);
  printf("\nOK\n");
  printf("------------------------------------\n");


  printf("getting test1:\n");
  if (!citylist_get(l, "test1", &c)) {
    printf("TEST FAILED: test1 not found.\n");
    return EXIT_FAILURE;
  }
  if (!city_equals(c, test1bis)) {
    printf("TEST FAILED: cities are not equals\n");
    city_print(c);
    printf("\tand\n");
    city_print(test1bis);
    return EXIT_FAILURE;
  }
  
  city_print(c);
  printf("\nOK\n");
  printf("------------------------------------\n");


  printf("remove test1:\n");
  l = citylist_remove(l, "test1");
  printf("\nOK\n");
  printf("------------------------------------\n");


  printf("getting test1:\n");
  if (!citylist_get(l, "test1", &c)) {
    printf("TEST FAILED: test1 not found.\n");
    return EXIT_FAILURE;
  }
  if (!city_equals(c, test1)) {
    printf("TEST FAILED: cities are not equals\n");
    city_print(c);
    printf("\tand\n");
    city_print(test1);
    return EXIT_FAILURE;
  }
  
  city_print(c);
  printf("\nOK\n");
  printf("------------------------------------\n");



  printf("\n\n\nTEST SUCCEEDED. :)\n");
  
  return EXIT_SUCCESS;
}