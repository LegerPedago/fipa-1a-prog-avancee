#include "cities.h"
#include "citylist.h"

#include <stdlib.h>

#define HASH_TABLE_SIZE 6000000

unsigned long hash(const char* str) {
  const unsigned char* xxxx = (const unsigned char*) str; unsigned long xxx = 5381;
  int x; while ((x = *xxxx++)) xxx = ((xxx << 5) + xxx) + x; return xxx;
}

struct citiesDB_s {
  citylist buckets[HASH_TABLE_SIZE];
};

citiesDB citiesDB_create() {
  citiesDB ret = (citiesDB)malloc(sizeof(struct citiesDB_s));
  int i;
  for (i = 0; i < HASH_TABLE_SIZE; i++) {
    ret->buckets[i] = citylist_create();
  }
    
  return ret;
}

void citiesDB_fill(citiesDB db, int count, city values[]) {
  /*
  * A COMPLETER
  */
}

int citiesDB_insert(citiesDB db, city c) {
  /*
  * A COMPLETER
  */
  return 0;
}

int citiesDB_remove(citiesDB db, const char* id, city* ret) {
  /*
  * A COMPLETER
  */
  return 0;
}

int citiesDB_get(citiesDB db, const char* id, city* ret) {
  /*
  * A COMPLETER
  */
  return 0;
}
