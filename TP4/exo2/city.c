#include "city.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void city_print(city c) {
  printf("id: %s\nname: %s\ncountry: %s\nlongitude, latitude: (%lf, %lf)\n",
    c.id, c.name, c.country, c.longitude, c.latitude);
}

int string_equals(const char* str1, const char* str2) {
  return (str1 == NULL && str2 == NULL)
    || (str1 != NULL && str2 != NULL && !strcmp(str1, str2));
}

int city_equals(city c1, city c2) {
  return string_equals(c1.id, c2.id)
    && string_equals(c1.name, c2.name)
    && string_equals(c1.country, c2.country)
    && fabs(c1.longitude - c2.longitude) < 1e-10
    && fabs(c1.latitude - c2.latitude) < 1e-10;
}

city city_clone(city c) {
  city ret;
  char* buf;
  
  if (c.id != NULL) {
    buf = malloc(strlen(c.id));
    strcpy(buf, c.id);
    ret.id = buf;
  }
  else {
    ret.id = NULL;
  }
  
  if (c.name != NULL) {
    buf = malloc(strlen(c.name));
    strcpy(buf, c.name);
    ret.name = buf;
  }
  else {
    ret.id = NULL;
  }
  
  if (c.country != NULL) {
    buf = malloc(strlen(c.country));
    strcpy(buf, c.country);
    ret.country = buf;
  }
  else {
    ret.country = NULL;
  }
  
  ret.longitude = c.longitude;
  ret.latitude = c.latitude;
  
  return ret;
}