#include <stdio.h>
#include <stdlib.h>

#include "cities.h"
#include "loader.h"

int main() {
  citiesDB db = citiesDB_load();
  city c;
  
  printf("\nOK\n");
  printf("------------------------------------\n");
  
  c.id = "masuperville";
  c.name = "Super ville";
  
  printf("inserting:\n");
  city_print(c);
  if (!citiesDB_insert(db, c)) {
    printf("TEST FAILED: insertion failed.\n");
    return EXIT_FAILURE;
  }
  
  printf("\nOK\n");
  printf("------------------------------------\n");
  
  c.name = "Super ville II";
  
  printf("inserting:\n");
  city_print(c);
  if (citiesDB_insert(db, c)) {
    printf("TEST FAILED: insertion should fail for multiple id.\n");
    return EXIT_FAILURE;
  }
  
  printf("\nOK (insertion failed for multiple id)\n");
  printf("------------------------------------\n");  
  
  printf("getting: evry\n");
  if (!citiesDB_get(db, "evry", &c)) {
    printf("TEST FAILED: evry not found.\n");
    return EXIT_FAILURE;
  }
  
  city_print(c);
  printf("\nOK\n");
  printf("------------------------------------\n");
  
  
  printf("removing: strasbourg\n");
  if (!citiesDB_remove(db, "strasbourg", &c)) {
    printf("TEST FAILED: strasbourg not found.\n");
    return EXIT_FAILURE;
  }
  
  city_print(c);
  printf("\nOK\n");
  printf("------------------------------------\n");

  
  printf("getting: strasbourg\n");
  if (citiesDB_get(db, "strasbourg", NULL)) {
    printf("TEST FAILED: strasbourg should not be found.\n");
    return EXIT_FAILURE;
  }

  printf("not found\n");
  printf("\nOK\n");
  printf("------------------------------------\n");
  
  
  printf("\n\n\nTEST SUCCEEDED. :)\n");
  
  return EXIT_SUCCESS;
}