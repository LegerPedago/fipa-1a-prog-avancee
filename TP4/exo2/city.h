#ifndef CITY_H
#define CITY_H

typedef struct {
	const char* id;
	const char* name;
	const char* country;
	double longitude;
	double latitude;
} city;

/*	@ensures: Affiche la ville c sur la sortie standard. */
void city_print(city c);

/*	@ensures: Test l'egalite des contenus de deux villes. */
int city_equals(city c1, city c2);

/*	@ensures: Renvoie une nouvelle ville avec le contenu copie. */
city city_clone(city c);

#endif