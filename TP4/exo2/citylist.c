#include "citylist.h"

#include <stdlib.h>
#include <string.h>

struct citynode {
  city value;
  citylist next;
};

citylist citylist_create() {
  return NULL;
}

int citylist_isEmpty(citylist l) {
  return l == NULL;
}

citylist citylist_add(citylist l, city value) {
  citylist ret = (citylist)malloc(sizeof(struct citynode));
  ret->value = value;
  ret->next = l;
  return ret;
}

citylist citylist_remove(citylist l, const char* id) {
  citylist previous = NULL;
  citylist current = l;
  while (current != NULL) {
    if (!strcmp(current->value.id, id)) {
      if (previous == NULL) {
        previous = current;
        current = current->next;
        previous->next = NULL;
        free(previous);
        return current;
      }
      else {
        previous->next = current->next;
        current->next = NULL;
        free(current);
        return l;
      }
    }
    previous = current;
    current = current->next;
  }
  return l;
}

int citylist_get(citylist l, const char* id, city* ret) {
  /*
  * A COMPLETER
  */
  return 0;
}
