#ifndef CITIES_H
#define CITIES_H

#include "city.h"

/* Definition du type de la table des villes. */
typedef struct citiesDB_s* citiesDB;

/*	@ensures: Retourne une table vide. */
citiesDB citiesDB_create();

/*	@requires: db bien formee et non NULL.
		values est un tableau de longueur au moins count et
		ne possedant pas deux villes ayant les meme id.
	@assigns: db
	@ensures: Remplie la table des 'count' premieres villes
		du tableau values SANS VERIFIER l'unicite de l'id. */
void citiesDB_fill(citiesDB db, int count, city values[]);

/*	@requires: db bien formee et non NULL.
	@assigns: db
	@ensures: Insere la ville c dans la table db.
		Retourne 0 si une ville avec le meme id existe deja dans la table,
		un entier non nul sinon. */
int citiesDB_insert(citiesDB db, city c);

/*	@requires: db bien formee et non NULL.
	@assigns: db, ret
	@ensures: Retire la ville correspondante a l'id passe en parametre
		dans la table db. Si la ville est trouvee, elle est assignee au contenu
		de ret si le pointeur n'est pas NULL.
		Retourne 0 si la ville n'a pas ete trouvee, un entier non nul sinon. */
int citiesDB_remove(citiesDB db, const char* id, city* ret);

/*	@requires: db bien formee et non NULL.
	@assigns: ret
	@ensures: Cherche la ville correspondante à l'id passe en parametre
		dans la table db. Si la ville est trouvee, elle est assignee au contenu
		de ret si le pointeur n'est pas NULL.
		Retourne 0 si la ville n'a pas ete trouvee, un entier non nul sinon. */
int citiesDB_get(citiesDB db, const char* id, city* ret);

#endif