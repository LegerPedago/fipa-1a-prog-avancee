#include "cities.h"
#include "citylist.h"

#include <stdlib.h>

struct citiesDB_s {
  citylist list;
};

citiesDB citiesDB_create() {
  citiesDB ret = (citiesDB)malloc(sizeof(struct citiesDB_s));
  ret->list = citylist_create();
  
  return ret;
}

void citiesDB_fill(citiesDB db, int count, city values[]) {
  /*
  * A COMPLETER
  */
}

int citiesDB_insert(citiesDB db, city c) {
  /*
  * A COMPLETER
  */
  return 0;
}

int citiesDB_remove(citiesDB db, const char* id, city* ret) {
  /*
  * A COMPLETER
  */
  return 0;
}

int citiesDB_get(citiesDB db, const char* id, city* ret) {
  /*
  * A COMPLETER
  */
  return 0;
}