#include <stdlib.h>
#include <stdio.h>
#include <time.h> 

/* Pour l'instant on ne se soucie pas de la mémoire, 
** on pourrait optimiser cela en évitant certaines copies.
*/

typedef struct node{
  int val;
  struct node* next;
} node;

typedef node* list;

/*Initialisation*/
list list_create(){
  return NULL;
}

/*Test de vacuité*/
int list_isEmpty(list l){
  return l == NULL;
}

/*Ajout en début de liste*/
list list_prepend(list l, int x){
  list newl = malloc(sizeof(node));
  newl->val = x;
  newl->next = l;
  return newl;
}

/*Ajout en début de liste PAR EFFET*/
void list_prependE(list* pl, int x){
  *pl = list_prepend(*pl, x);
}

/*Suppression du premier élément*/
list list_tail(list l){
  list newl = l->next;
  free(l);
  return newl;
}

/*Suppression de la première occurence de l'élément x*/
list list_remove(list l, int x){
  list curr = l;
  int found = 0;
  if(list_isEmpty(curr)) return NULL;
  if(curr->val == x) return list_tail(l);
  while(!list_isEmpty(curr->next) && !found){
    if(curr->next->val == x){
      list tmp = curr->next;
      curr->next = tmp->next;
      free(tmp);
      found = 1;
    }
    else curr = curr->next;
  }
  return l;
}

/*Suppression de la première occurence de l'élément x PAR EFFET*/
void list_removeE(list* pl, int x){
  *pl = list_remove(*pl, x);
}

/*Affichage*/
void list_print(list l){
  list curr = l;
  while(!list_isEmpty(curr->next)){
    printf("%d -> ", curr->val);
    curr = curr->next;
  }
  printf("%d\n", curr->val);
}

/*Test d'appartenance*/
int list_contains(list l, int x){
  list curr = l;
  int res = 0;
  while(!list_isEmpty(curr) && !res){
    res = curr->val == x;
    curr = curr->next;
  }
  return res;
}

/*Ajout en fin de liste*/
list list_append(list l, int x){
  list newl = malloc(sizeof(node));
  list curr = newl;
  while(!list_isEmpty(l)){
    curr->val = l->val;
    curr->next = malloc(sizeof(node));
    curr = curr->next;
    l = l->next;
  }
  curr->val = x;
  curr->next = NULL;
  return newl;
}

/*
** TRIS
*/

/*enlever le plus petit élément d'une liste *pl 
  et retourner cet élément isolé*/
list list_removeMin(list* pl){
  list minL = *pl;
  list curr = *pl;
  list prev = NULL;
  if(list_isEmpty(curr)) return NULL;
  while(!list_isEmpty(curr->next)){
    if(curr->next->val < minL->val){
      minL = curr->next;
      prev = curr;
    }
    curr = curr->next;
  }
  if(prev != NULL) prev->next = minL->next;
  else *pl = minL->next;
  minL->next = NULL;
  return minL;
}

/*tri par sélection*/
void list_selectSort(list* pl){
  list l = *pl;
  list sorted = list_create();
  while(!list_isEmpty(l)){
    list minL = list_removeMin(&l);
    if(list_isEmpty(sorted)){
      sorted = minL;
      *pl = sorted;
    }
    else{
      sorted->next = minL;
      sorted = minL;
    }
  }
}

/*Tests*/
int main(int argc, char* argv[]){
  list l = list_create();
  l = list_prepend(l,1);
  l = list_prepend(l,2);
  l = list_prepend(l,3);
  printf("l = ");
  list_print(l);

  l = list_tail(l);
  printf("list_tail(l) = ");
  list_print(l);

  printf("list_contains(l,2) = %d\n", list_contains(l,2));
  printf("list_contains(l,4) = %d\n", list_contains(l,4));

  l = list_append(l,6);
  printf("list_append(l,6) = ");
  list_print(l);

  list_selectSort(&l);
  printf("liste l triée : ");
  list_print(l);

  l = list_remove(l,1);
  printf("list_remove(l,1) = ");
  list_print(l);
  return 0;
}