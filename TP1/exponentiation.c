#include <stdlib.h>
#include <stdio.h>
#include <time.h> 

/*Méthode naïve, version itérative*/
double puissIter(double x, int n){
  double y = 1;
  int i = n;
  while(i > 0){
    y = y * x;
    i = i - 1;
  }
  return y;
}

/*Méthode naïve, version récursive*/
double puissRec(double x, int n, double acc){
  if(n == 1){
    return acc;
  }
  return puissRec(x, n - 1, acc * x);
}

/*Méthode plus efficace, version itérative*/
double puiss2Iter(double x, int n){
  double y = 1;
  int i = n;
  while(i > 0){
    if(i % 2 != 0){
      y = y * x;
    }
    x = x * x;
    i = i / 2;
  }
  return y;
}

/*Méthode plus efficace, version récursive*/
double puiss2Rec(double x, int n, double acc){
  if(n == 1){
    return acc;
  }
  else if(n % 2 != 0){
    return puiss2Rec(x, n - 1, acc * x);
  }
  else{
    return puiss2Rec(x * x, n / 2, acc * x);
  }
}

/*Tests dans la fonction main*/
int main(int argc, char* argv[]){
  double x = 6.2;
  int n = 2000000000;
  clock_t t;
  double res;

  /* Test de la version itérative naïve */
  t = clock(); 
  res = puissIter(x,n);
  t = clock() - t;
  printf("iterative result : %f - time taken : %f seconds\n", res, ((double)t)/CLOCKS_PER_SEC);

  /* Test de la version récursive naïve
  ** Ce test provoque une erreur de segmentation due à un dépassement de pile,
  ** ce qui n'est pas étonnant vu qu'on a 2 milliards d'appels récursifs */
  /*
  t = clock(); 
  res = puissRec(x,n,x);
  t = clock() - t;
  printf("recursive result : %f - time taken : %f seconds\n", res, ((double)t)/CLOCKS_PER_SEC);
  */

  /* Test de la version itérative plus efficace */
  t = clock(); 
  res = puiss2Iter(x,n);
  t = clock() - t;
  printf("iterative result 2: %f - time taken : %f seconds\n", res, ((double)t)/CLOCKS_PER_SEC);

  /* Test de la version récursive plus efficace */
  t = clock(); 
  res = puiss2Rec(x,n,x);
  t = clock() - t;
  printf("recursive result 2: %f - time taken : %f seconds\n", res, ((double)t)/CLOCKS_PER_SEC);
  
  return 0;
}