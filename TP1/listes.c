#include <stdlib.h>
#include <stdio.h>
#include <time.h> 

/* Pour l'instant on ne se soucie pas de la mémoire, 
** on pourrait optimiser cela en évitant certaines copies.
*/

typedef struct node{
  int val;
  struct node* next;
} node;

typedef node* list;

/*Initialisation*/
list list_create(){
  return NULL;
}

/*Test de vacuité*/
int list_isEmpty(list l){
  return l == NULL;
}

/*Ajout en début de liste*/
list list_prepend(list l, int x){
  list newl = malloc(sizeof(node));
  newl->val = x;
  newl->next = l;
  return newl;
}

/*Suppression du premier élément*/
list list_tail(list l){
  list newl = l->next;
  free(l);
  return newl;
}

/*Affichage*/
void list_print(list l){
  list curr = l;
  while(!list_isEmpty(curr->next)){
    printf("%d -> ", curr->val);
    curr = curr->next;
  }
  printf("%d\n", curr->val);
}

/*Test d'appartenance*/
int list_contains(list l, int x){
  list curr = l;
  int res = 0;
  while(!list_isEmpty(curr) && !res){
    res = curr->val == x;
    curr = curr->next;
  }
  return res;
}

/*Ajout en fin de liste*/
list list_append(list l, int x){
  list newl = malloc(sizeof(node));
  list curr = newl;
  while(!list_isEmpty(l)){
    curr->val = l->val;
    curr->next = malloc(sizeof(node));
    curr = curr->next;
    l = l->next;
  }
  curr->val = x;
  curr->next = NULL;
  return newl;
}

/*Tests*/
int main(int argc, char* argv[]){
  list l = list_create();
  l = list_prepend(l,1);
  l = list_prepend(l,2);
  l = list_prepend(l,3);
  printf("l = ");
  list_print(l);

  l = list_tail(l);
  printf("list_tail(l) = ");
  list_print(l);

  printf("list_contains(l,2) = %d\n", list_contains(l,2));
  printf("list_contains(l,4) = %d\n", list_contains(l,4));

  printf("list_append(l,6) = ");
  list_print(list_append(l,6));
  return 0;
}
