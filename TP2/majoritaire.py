# coding: utf-8
#!/usr/bin/python
 
import csv
import time

#QUESTION 1: algorithme naïf
def count(x, l):
  i = 0
  for e in l:
    if e==x: i += 1
  return i

def majoritaire(l):
  n = len(l)
  for e in l:
    if count(e,l) > n/2: 
      return e
  return

#Bonus: petite optimisation (enregistrement des éléments déjà comptés)
#       on gagne pas grand chose mais bon...
def majoritaire2(l):
  n = len(l)
  vu = []
  for e in l:
    if e not in vu:
      if count(e,l) > n/2: 
        return e
  return

#QUESTION 2: divide and conquer
def majoritaire_divide(l):
  n = len(l)
  if n == 1:
    return (l[0],1)
  else:
    l1 = l[0:n/2]
    l2 = l[n/2:n]
    (x,nx) = majoritaire_divide(l1)
    (y,ny) = majoritaire_divide(l2)
    if nx != 0:
      nx += count(x,l2)
    if ny != 0:
      ny += count(y,l1)
    if nx > n/2:
      return (x,nx)
    elif ny > n/2:
      return (y,ny)
    else:
      return(None,0)

#QUESTION 3: optimisation
def candidat(l):
  n = len(l)
  if n == 1:
    return (l[0],1)
  else:
    l1 = l[0:n/2]
    l2 = l[n/2:n]
    (x, nx) = candidat(l1)
    (y, ny) = candidat(l2)
    if x == y == None:
      return (None,0)
    elif x == None:
      return (y, ny + n/4)
    elif y == None:
      return (x, nx + n/4)
    else:
      if x == y:
        return (x, nx + ny)
      elif nx > ny:
        return (x, nx + n/2 - ny)
      elif ny > nx:
        return (y, ny + n/2 - nx)
      else:
        return (None, 0)

def majoritaire_opti(l):
  c = candidat(l)[0]
  if count(c,l) > len(l)/2:
    return c
  else:
    return

#########################################################
#Tests:

#fname = "ssbu_10.csv"
#fname = "ssbu_10k.csv"
fname = "ssbu_1M.csv"
file = open(fname, "rb")
start = time.time()

try:
  reader = csv.reader(file)
  for row in reader:
    start = time.time()
    #print majoritaire(row)
    #print majoritaire2(row)
    #print majoritaire_divide(row)
    print majoritaire_opti(row)
    end = time.time()
    print "Elapsed time: " + str((end - start)*1) + "s"

finally:
  file.close()